<?php

class HomeMoneyer{

	private $curl;

	private $clientId;

	private $clientSecret;

	private $username;

	private $password;

	private $accessToken;

	private $apiUrl = 'https://homemoney.ua/api/api2.asmx/';

	private $urlMethod = '';

	private $errors = array();

	private $data = array();


	const TYPE_EXPENSE = 1;
	const TYPE_TRANSFER = 2;
	const TYPE_INCOME = 3;

	public function init(){
		$this->curl = new Curl;
		$this->curl->setOption(CURLOPT_VERBOSE, false);
	}

	public function getUserInfo(){
		$this->urlMethod = 'UserInfo';
		$this->makeRequest();
	}


	public function getBalanceList(){
		$this->urlMethod = 'BalanceList';
		$this->makeRequest();
	}


	public function getCategoryList(){
		$this->urlMethod = 'CategoryList';
		$this->makeRequest();
	}

	public function getTransactionList($count){
		$this->urlMethod = 'TransactionList ';
		$params = array('TopCount'=>$count);
		$this->makeRequest($params);
	}

	private function checkAuth(){
		if (is_null($this->getAccessToken())){
			$this->loadAccessToken();
			if (is_null($this->getAccessToken())){
				throw new Exception('Error get access token');
			}
		}
	}


	private function makeRequest($params = array()){
		$this->checkAuth();
		$url = $this->getApiUrl() . '/' . $this->getUrlMethod();
		$params['token'] = $this->getAccessToken();

		$this->clean();
		$output = $this->curl->get($url, $params);
		$outputData = json_decode($output, true);
		$this->setErrors($outputData['Error']);
		unset($outputData['Error']);
		$this->setData($outputData);
	}


	private function loadAccessToken(){
		$params = array();
		$params['username'] = $this->getUsername();
		$params['password'] = $this->getPassword();
		$params['client_id'] = $this->getClientId();
		$params['client_secret'] = $this->getClientSecret();
		$url = $this->getApiUrl() . '/TokenPassword';
		$output = $this->curl->get($url, $params);
		if ($this->isJson($output)){
			$outputData = json_decode($output, true);
			$this->setAccessToken($outputData['access_token']);
		} else {
			throw new Exception('No valid response');
		}
	}

	private function setAccessToken($accessToken){
		file_put_contents('/tmp/hm.token', $accessToken);
		$this->accessToken = $accessToken;
	}

	private function getAccessToken(){
		$accessToken = $this->accessToken;
		/**
		 * FIXME:
		 * @author Sergii "Patricy" Kytsiuk <skitsyuk@patricy.com.ua>
		 */
		if (is_null($accessToken) && file_exists('/tmp/hm.token')){
			$accessToken = file_get_contents('/tmp/hm.token');
		}

		return $accessToken;
	}

	private function clean(){
		$this->setErrors(array());
		$this->setData(array());
	}

	private function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	public function getClientId() {
		return $this->clientId;
	}

	public function getClientSecret() {
		return $this->clientSecret;
	}

	public function getUsername() {
		return $this->username;
	}

	public function getPassword() {
		return $this->password;
	}

	public function getApiUrl() {
		return $this->apiUrl;
	}

	public function getErrors() {
		return $this->errors;
	}

	public function getData() {
		return $this->data;
	}

	public function setClientId($clientId) {
		$this->clientId = $clientId;
	}

	public function setClientSecret($clientSecret) {
		$this->clientSecret = $clientSecret;
	}

	public function setUsername($username) {
		$this->username = $username;
	}

	public function setPassword($password) {
		$this->password = $password;
	}

	public function setApiUrl($url) {
		$this->apiUrl = $url;
	}

	public function setErrors($errors) {
		$this->errors = $errors;
	}

	public function setData($data) {
		$this->data = $data;
	}

	public function getUrlMethod() {
		return $this->urlMethod;
	}

	public function setUrlMethod($urlMethod) {
		$this->urlMethod = $urlMethod;
	}
}
